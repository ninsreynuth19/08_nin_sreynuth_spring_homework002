package com.example.pp_08_nin_sreynuth_spring_homework002.service;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.ProductRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;

import java.util.List;


public interface ProductService  {

    Integer insertProduct(Product e);

    List<Product> getAllProduct();

    Product getProductByID(Integer id);

    void updateProductByID(Integer id, ProductRequest productRequest);

    void deleteProductByID(Integer id);
}
