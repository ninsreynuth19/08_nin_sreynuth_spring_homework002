package com.example.pp_08_nin_sreynuth_spring_homework002.service;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.ProductRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;
import com.example.pp_08_nin_sreynuth_spring_homework002.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public class ProductServiceImplement implements ProductService{

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Integer insertProduct(Product e) {
        return productRepository.insertProduct(e);
    }
    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductByID(Integer id) {
        return productRepository.getProductByID(id);
    }

    @Override
    public void updateProductByID(Integer id, ProductRequest productRequest) {
        productRepository.updateProductByID(id,productRequest);
    }

    @Override
    public void deleteProductByID(Integer id) {
        productRepository.deleteProductByID(id);
    }
}
