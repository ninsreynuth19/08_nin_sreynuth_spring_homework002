package com.example.pp_08_nin_sreynuth_spring_homework002.service;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.CustomerRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Customer;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;
import com.example.pp_08_nin_sreynuth_spring_homework002.repository.CustomerRepository;
import com.example.pp_08_nin_sreynuth_spring_homework002.repository.ProductRepository;

import java.util.List;

public interface CustomerService {

    Integer insertCustomer(Customer c);

    List<Customer> getAllCustomer();

    Customer getCustomerByID(Integer id);

    void updateCustomerByID(Integer id, CustomerRequest customerRequest);

    void deleteCustomerByID(Integer id);
}
