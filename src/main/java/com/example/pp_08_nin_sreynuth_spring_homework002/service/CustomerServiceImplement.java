package com.example.pp_08_nin_sreynuth_spring_homework002.service;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.CustomerRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Customer;
import com.example.pp_08_nin_sreynuth_spring_homework002.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public class CustomerServiceImplement implements CustomerService{
    private CustomerRepository customerRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Integer insertCustomer(Customer c) {
        return customerRepository.insertCustomer(c);
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerByID(Integer id) {
        return customerRepository.getCustomerByID(id);
    }

    @Override
    public void updateCustomerByID(Integer id, CustomerRequest customerRequest) {
        customerRepository.updateCustomerByID(id,customerRequest);
    }

    @Override
    public void deleteCustomerByID(Integer id) {
        customerRepository.deleteCustomerByID(id);
    }
}
