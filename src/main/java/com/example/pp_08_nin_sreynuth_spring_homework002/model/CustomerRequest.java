package com.example.pp_08_nin_sreynuth_spring_homework002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerRequest {
    private Integer customer_id;
    private String customer_name;
    private double customer_address;
    private Number customer_phone;
}
