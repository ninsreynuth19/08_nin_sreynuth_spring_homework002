package com.example.pp_08_nin_sreynuth_spring_homework002.model.entity;

import java.util.Date;
import java.util.List;

public class Invoice {
    private Integer invoice_id;
    private Date invoice_date;
    private Customer customer_id;
    private List<InvoiceDetail> invoiceDetails;
}
