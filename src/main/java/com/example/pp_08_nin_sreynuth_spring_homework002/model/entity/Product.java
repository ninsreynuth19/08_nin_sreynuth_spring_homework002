package com.example.pp_08_nin_sreynuth_spring_homework002.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer product_id;
    private String product_name;
    private double product_price;
}
