package com.example.pp_08_nin_sreynuth_spring_homework002.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customer_id;
    private String customer_name;
    private double customer_address;
    private Number customer_phone;
}
