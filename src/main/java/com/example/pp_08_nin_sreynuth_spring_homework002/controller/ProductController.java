package com.example.pp_08_nin_sreynuth_spring_homework002.controller;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.ProductRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.respone.ApiRespone;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;
import com.example.pp_08_nin_sreynuth_spring_homework002.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    public List<Product> getAllProduct(){
        return productService.getAllProduct();
    }

    @GetMapping("/get-all-product/{id}")
    public ResponseEntity<?> getProductByID(@PathVariable Integer id) {
        Product product = productService.getProductByID(id);
        ApiRespone<Product> respone = new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                product
        );
        return new ResponseEntity<>(respone, HttpStatus.OK);
    }
//    Noted
    @PostMapping("/add-new-product")
    public ResponseEntity<?> insertProduct(@RequestBody Product e){
        e.setProduct_id(productService.insertProduct(e));
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                e

        ));

    }
    @PutMapping ("/update-product-by-id/{id}")
    public ResponseEntity<?> updateProductByID(@PathVariable Integer id, @RequestBody ProductRequest productRequest) {
        productService.updateProductByID(id,productRequest);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));

    }
    @DeleteMapping ("/update-product-by-id/{id}")
    public ResponseEntity<?> deleteProductByID(@PathVariable Integer id) {
        productService.deleteProductByID(id);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));

    }

}
