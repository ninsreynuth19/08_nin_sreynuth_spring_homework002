package com.example.pp_08_nin_sreynuth_spring_homework002.controller;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.CustomerRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.ProductRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Customer;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.respone.ApiRespone;
import com.example.pp_08_nin_sreynuth_spring_homework002.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    public List<Customer> getAllCustomer(){
        return customerService.getAllCustomer();
    }

    @PostMapping("/add-new-product")
    public ResponseEntity<?> insertCustomer(@RequestBody Customer c){
        c.setCustomer_id(customerService.insertCustomer(c));
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                c
        ));

    }

    @GetMapping("/get-all-customer/{id}")
    public ResponseEntity<?> getCustomerByID(@PathVariable Integer id) {
        Customer customer = customerService.getCustomerByID(id);
        ApiRespone<Customer> respone = new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                customer
        );
        return new ResponseEntity<>(respone, HttpStatus.OK);
    }
    @PutMapping ("/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerByID(@PathVariable Integer id, @RequestBody CustomerRequest customerRequest) {
        customerService.updateCustomerByID(id, customerRequest);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));

    }
    @DeleteMapping ("/update-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerByID(@PathVariable Integer id) {
        customerService.deleteCustomerByID(id);
        return ResponseEntity.ok(new ApiRespone<>(
                LocalDate.now(),
                HttpStatus.OK,
                "Successfully",
                null
        ));

    }
}
