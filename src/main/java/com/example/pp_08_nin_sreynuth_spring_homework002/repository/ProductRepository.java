package com.example.pp_08_nin_sreynuth_spring_homework002.repository;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.ProductRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProductRepository {
    @Select("""
            Select *
            from product_tb
            """)
    List<Product> getAllProduct();

    @Select("""
            select * from product_tb where product_id=#{id};
            """)
    Product getProductByID(Integer id);

    @Select("""
            insert into product_tb
            values (default,#{Pro.product_name},#{Pro.product_price})
            RETURNING product_id;
            """)
    Integer insertProduct(@Param("Pro") Product e);

    @Update("""
            update product_tb
            set product_name=#{Pro.product_name}, product_price=#{Pro.product_price}
            where product_id=#{id};
            """)
    void updateProductByID(Integer id,@Param("Pro") ProductRequest productRequest);

    @Delete("""
            delete from product_tb
            where product_id= #{id};
            """)
    void deleteProductByID(Integer id);
}
