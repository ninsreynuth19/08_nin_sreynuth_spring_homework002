package com.example.pp_08_nin_sreynuth_spring_homework002.repository;

import com.example.pp_08_nin_sreynuth_spring_homework002.model.CustomerRequest;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Customer;
import com.example.pp_08_nin_sreynuth_spring_homework002.model.entity.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface CustomerRepository {

    @Select("""
            select *
            from customer_tb
            """)
    List<Customer> getAllCustomer();


    @Select("""
            insert into customer_tb
            values (default,#{Customer.customer_name},#{Customer.customer_address}, #{Customer.customer_phone})
            RETURNING customer_id;
            """)
    Integer insertCustomer(@Param("Customer") Customer c);

    @Select("""
            select * from customer_tb where customer_id=#{id};
            """)
    Customer getCustomerByID(Integer id);

    @Update("""
            update customer_tb
            set customer_name=#{Customer.customer_name}, customer_addess=#{Customer.customer_address}, customer_phone= #{Customer.customer_phone}
            where customer_id=#{id};
            """)
    void updateCustomerByID(Integer id,@Param("Customer") CustomerRequest customerRequest);

    @Delete("""
            delete from customer_tb
            where customer_id=#{id};
                                    
            """)
    void deleteCustomerByID(Integer id);
}
