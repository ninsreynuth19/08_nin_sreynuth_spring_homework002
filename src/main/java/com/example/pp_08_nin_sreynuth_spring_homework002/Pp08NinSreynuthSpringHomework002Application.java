package com.example.pp_08_nin_sreynuth_spring_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pp08NinSreynuthSpringHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(Pp08NinSreynuthSpringHomework002Application.class, args);
    }

}
